from django.contrib import admin
from . import models
from .forms import CargoModeloForm

# Register your models here.

class Cargoadmin(admin.ModelAdmin):
     list_display = ["cargo_descripcion","estado_cargo","fecha_insercion", "cargo_id","email"]   #Muestra las columnas que queremos que se vean
     list_filter = ["fecha_insercion"]    #Genera un filtro que filtra por las fechas
     list_editable = ["estado_cargo","fecha_insercion","email"]     # Nose que puta hace, ya se, muestra para editar en la lista
     search_fields = ["cargo_descripcion"] #Genera un buscador para poder filtrar

     form = CargoModeloForm

admin.site.register(models.Cargo,Cargoadmin)

admin.site.register(models.Cliente)



