from django.db import models

# Create your models here.

class Cargo(models.Model):
    cargo_id = models.AutoField(primary_key=True)
    estado_cargo = models.BooleanField(null = True)
    cargo_descripcion = models.CharField(max_length=25)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16,null=True, blank=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=16,null=True)
    email = models.EmailField(max_length=254, null=True)

    def __str__(self):
        return self.cargo_descripcion
    
    class Meta:
        db_table = 'cargo'


prioridades_status = [
    
    (1,"Alta"),
    (2,"Media"),       #Posibles "choices" para las prioridades"
    (3,"Baja")

]

servicios_status = [
    
    (1,"Servicio_T"),
    (2,"Servicio_K"),   #Posibles "choices" para los servicios"
    (3,"Servicio_Q")

]
class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    cedula = models.CharField(null=True,blank=False,max_length=10)
    nombre = models.CharField(null = True,blank=False,max_length=25)
    apellido = models.CharField(null = True, blank=False,max_length=25)
    fecha_insercion = models.DateTimeField(null=True)
    prioridad = models.IntegerField(null=False , blank=False, choices = prioridades_status, default = 3 ) # En caso que sea 1 es de maxima prioridad, caso 2 media y caso 3 baja prioridad.
    servicio = models.IntegerField(null=False , blank=False, choices = servicios_status, default = 1 )
    ticket = models.CharField(null = True, blank=False,max_length=25)
    
    def __str__(self):
        return self.cedula
    
    class Meta:
        db_table = 'cliente'






