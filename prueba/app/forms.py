from django import forms

from .models import Cargo,Cliente

class CargoModeloForm(forms.ModelForm):
    class Meta:
        model =  Cargo
        fields =  ["cargo_descripcion","estado_cargo"]



class ContactForm(forms.Form):
    nombre = forms.CharField()
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)
                  


class ClienteModeloForm(forms.ModelForm):
    class Meta:
        model =  Cliente
        fields =  ["cedula","nombre","apellido","prioridad","servicio","ticket"]