from django.shortcuts import render
from .cargoForm import CargoFormulario
from .forms import CargoModeloForm, ContactForm, ClienteModeloForm
from .models import *
from django.conf import settings

import random

# Create your views here.

def Inicio(request):

    return render(request,"login.html")



def registro_cliente(request):
            
    return render(request, "registro.html")


def lista(request):
    
    listado_clientes = Cliente.objects.all()
    form = ClienteModeloForm(request.POST or None)

    context = {
        "form":form,
        "Titulo":"Formulario de Cliente",
        "listado_clientes":listado_clientes

    }
    
    return render(request, "listas.html",context)


def grabar(request):
    
    if request.method == 'POST':
        cedula = request.POST['cedula']
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        servicio = request.POST['servicio']
        prioridad = request.POST['prioridad']

        
        if (servicio == "1"):
            aux_servicio = "T  "

        if (servicio == "2"):
            aux_servicio = "K "

        if (servicio == "3"):
            aux_servicio = "Q "
        
        
        numeros = [] # Numeros es la lista donde almacenaremos los numeros de nuestros tickets.
        
        numero =  random.randint(1,100)
        while (numero in numeros):
            numero =  random.randint(1,100)
            
        numeros.append(numero)       # Agregamos a nuestra lista de numeros, el numero de nuestro ticket.
        numero_ticket = str(numero)
        
        
        #Prioridades => 1-ALTA     2-MEDIA    3-BAJA 
        
        
        obj = Cliente()
        obj.cedula = cedula
        obj.nombre = nombre
        obj.apellido = apellido
        obj.servicio = servicio
        obj.ticket = aux_servicio + numero_ticket
        obj.prioridad = prioridad
        
        obj.save()
        
    return render(request, "registro.html")


def eliminar(request,id_persona):
    
    persona = Cliente.objects.get(pk = id_persona)
    persona.delete() 
    persona = Cliente.objects.all()
        
    return render(request, "mensaje_eliminado.html")


def ordenar(vectorbs):
   
    n = 0 

    for _ in vectorbs:
        n += 1 
    
    for i in range(n-1): 

        for j in range(0, n-i-1): 

            if vectorbs[j] > vectorbs[j+1] :
                vectorbs[j], vectorbs[j+1] = vectorbs[j+1], vectorbs[j]



