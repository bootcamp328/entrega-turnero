from django import forms

from .models import Cargo


class CargoFormulario(forms.Form):
    descripcion = forms.CharField(max_length=20, required=True)
    estado = forms.BooleanField(required=False )
    
